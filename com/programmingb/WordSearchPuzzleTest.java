package com.programmingb;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by cormaclavery on 20/03/2017.
 */
public class WordSearchPuzzleTest {


    private WordSearchPuzzle wordSearchPuzzle;

    @Test
    public void WordSearchPuzzleGeneratesFromListOfWords(){
        wordSearchPuzzle = new WordSearchPuzzle(new ArrayList<>(Arrays.asList(new String[]{"word1, word2, word3"})));
        char[][] puzzle = wordSearchPuzzle.getPuzzleAsGrid();

        assertThat(puzzle.length * puzzle[0].length, is(36));
    }


}