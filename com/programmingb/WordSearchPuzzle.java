package com.programmingb;

import java.io.*;
import java.util.*;

/*
*  Created By:
*  - Cormac Lavery 16139658
*  - Hugh Tiernan 16145674
*  - Connell Harte 09005210
*  - Paul McCarthy 16137604
*
* */

public class WordSearchPuzzle {

    private char[][] puzzle;
    private char[][] solution;

    private List<String> puzzleWords;

    public WordSearchPuzzle(List<String> userSpecifiedWords) {

        puzzleWords = new ArrayList<>(userSpecifiedWords);

        int size = calculateGridSize(puzzleWords);
        puzzle = new char[size][size];


        generateWordSearchPuzzle(puzzleWords);


    }

    public WordSearchPuzzle(String wordFile, int wordCount, int shortest, int longest) {

        try {
            BufferedReader reader = new BufferedReader(new FileReader(wordFile));
            List<String> savedWords = new ArrayList<>();
            Set<String> uniqueWords = new HashSet<>();
            String lineFromFile;

            //read lines from file and save to savedWords list if match the criteria
            while ((lineFromFile = reader.readLine()) != null) {
                if (lineFromFile.length() >= shortest && lineFromFile.length() <= longest) {
                    savedWords.add(lineFromFile.toLowerCase().trim());
                }
            }

            //now keep looking adding words to the set until it has either enough unique words or has taken every word which matches the criteria
            while (uniqueWords.size() < wordCount && uniqueWords.size() < savedWords.size()) {
                int randomWordIndex = (int) (Math.random() * savedWords.size());
                String word = savedWords.get(randomWordIndex);
                uniqueWords.add(word);
            }

            //copy into
            puzzleWords = new ArrayList<>(uniqueWords);


            int size = calculateGridSize(puzzleWords);


            puzzle = new char[size][size];


            generateWordSearchPuzzle(puzzleWords);

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private int calculateGridSize(List<String> words) {
        //sum words
        int sum = 0;
        //remember max size as this will be the minimum size of the wordsearch
        int max = 0;
        for (String word : puzzleWords) {
            if (word.length() > max) max = word.length();
            sum += word.length();
        }
        //apply a appropriate scaling factor
        double scalingFactor = 1.75;

        sum = (int) (Math.sqrt((double) sum * scalingFactor) + 1);
        int size = sum > max ? sum : max;
        return size;
    }

    private void generateWordSearchPuzzle(List<String> words) {

        int randomRow = (int) (Math.random() * puzzle.length);
        int randomCol = (int) (Math.random() * puzzle[0].length);
        int randomDirection = (int) (Math.random() * 4); // 0 = left-right, 1 = right-left, 2 = up-down, 3 = down-up

        //now attempt to randomly place words on puzzle grid
        for (String word : words) {
            int retries = 0;
            while (!validPostion(word, randomRow, randomCol, randomDirection)) {
                //if it was not a valid position then chose another random column, row and direction
                randomRow = (int) (Math.random() * puzzle.length);
                randomCol = (int) (Math.random() * puzzle[0].length);
                randomDirection = (int) (Math.random() * 4);
                retries++;

                //if we randomly try more places than there are grids on the puzzle..
                if (retries > puzzle.length * puzzle[0].length) {
                    //...we will first systematically try and place it in all squares
                    retries = 0;
                    boolean possible = fillFirst(word);
                    if (!possible) {
                        //...then we will resize the grid and try again
                        resizePuzzle();
                    } else {
                        break;//and reset retries
                    }

                }

            }

        }
        //before we fill with random characters we save a copy of the solution
        solution = new char[puzzle.length][puzzle.length];
        for (int i = 0; i < puzzle.length; i++) {
            System.arraycopy(puzzle[i], 0, solution[i], 0, puzzle.length);
        }
        //fill in with random characters
        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle[0].length; j++) {
                if (puzzle[i][j] == '\u0000') {
                    puzzle[i][j] = generateRandomChar();
                }
            }
        }

    }


    //methods attempts to place given word at random row and column position and random  direction and then returns whether attempt was successful or not
    private boolean validPostion(String word, int randomRow, int randomCol, int randomDirection) {

        switch (randomDirection) {
            case (0): //left-right
                //first check if space to the right of randomCol is large enough for the word
                if (puzzle[randomRow].length - randomCol < word.length()) {
                    return false;
                }
                //starting at the random column we check right to see if the space is free or is equal to the letter at that point in the word
                for (int i = randomCol; i < randomCol + word.length(); i++) {
                    if (puzzle[randomRow][i] != '\u0000' && puzzle[randomRow][i] != word.charAt(i - randomCol)) {
                        return false;
                    }
                }

                //if it has gotten to this point then we know we can place the string so we do so
                for (int i = randomCol; i < randomCol + word.length(); i++) {
                    puzzle[randomRow][i] = word.charAt(i - randomCol);
                }

                break;

            case (1): //right-left
                //first check if space to the left of randomCol is large enough for word
                if (randomCol + 1 < word.length()) {
                    return false;
                }
                //starting at the random column we check left to see if the space is free or is equal to the letter at that point in the word
                int wordPos = 0;
                for (int i = randomCol; i > randomCol - word.length(); i--) {
                    if (puzzle[randomRow][i] != '\u0000' && puzzle[randomRow][i] != word.charAt(wordPos)) {
                        return false;
                    }
                    wordPos++;
                }

                //if it has gotten to this point we know we can place the string so we do
                wordPos = 0;
                for (int i = randomCol; i > randomCol - word.length(); i--) {
                    puzzle[randomRow][i] = word.charAt(wordPos);
                    wordPos++;
                }

                break;

            case (2): //up-down
                //first check if enough space below the random row
                if (puzzle.length - randomRow < word.length()) {
                    return false;
                }
                //then check to see if all the spaces below are blank or match the letter at that words position;
                for (int i = randomRow; i < randomRow + word.length(); i++) {
                    if (puzzle[i][randomCol] != '\u0000' && puzzle[i][randomCol] != word.charAt(i - randomRow)) {
                        return false;
                    }
                }
                //if it has gotten to this point we know we can place the string so we do
                for (int i = randomRow; i < randomRow + word.length(); i++) {
                    puzzle[i][randomCol] = word.charAt(i - randomRow);
                }

                break;

            case (3): //down-up
                //first check if enough space above the random row
                if (randomRow + 1 < word.length()) {
                    return false;
                }
                //then check if all spaces above are blank or match the character we want to insert
                wordPos = 0;
                for (int i = randomRow; i > randomRow - word.length(); i--) {
                    if (puzzle[i][randomCol] != '\u0000' && puzzle[i][randomCol] != word.charAt(wordPos)) {
                        return false;
                    }
                }

                //if we have gotten to this point we know that we can place the string so we do

                wordPos = 0;
                for (int i = randomRow; i > randomRow - word.length(); i--) {
                    puzzle[i][randomCol] = word.charAt(wordPos);
                    wordPos++;
                }


                break;
        }

        return true;
    }

    private boolean fillFirst(String word) {
        //This method will iterate through grid looking for the first location and direction that we will run through
        for (int i = 0; i < puzzle.length; i++) {
            for (int j = 0; j < puzzle[0].length; j++) {
                for (int k = 0; k < 4; k++) {
                    if (validPostion(word, i, j, k)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void resizePuzzle() {
        char[][] resizedPuzzle = new char[puzzle.length + 1][puzzle[0].length + 1];
        for (int i = 0; i < puzzle.length; i++) {
            System.arraycopy(puzzle[i], 0, resizedPuzzle[i], 0, puzzle.length);
        }
        puzzle = resizedPuzzle;

    }

    private char generateRandomChar() {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        return alphabet.charAt((int) (Math.random() * alphabet.length()));
    }


    public char[][] getPuzzleAsGrid() {
        return puzzle;
    }

    public List<String> getWordSearchList() {
        return puzzleWords;
    }

    public void showWordSearchPuzzle() {
        for (char[] row : puzzle) {
            for (Character character : row) {
                System.out.print("[" + character + "]");
            }
            System.out.println();
        }
        System.out.println();
        System.out.print(getPuzzleAsString());
        System.out.println();
    }

    public void showWordSearchSolution() {
        for (char[] row : solution) {
            for (Character character : row) {
                System.out.print("[" + character + "]");
            }
            System.out.println();
        }

    }

    public String getPuzzleAsString() {
        String result = "";
        for (String word : puzzleWords) {
            result += word + " \n";
        }
        return result;
    }


}