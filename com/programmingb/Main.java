package com.programmingb;

import java.io.*;
import java.util.*;

/*
*  Created By:
*  - Cormac Lavery 16139658
*  - Hugh Tiernan 16145674
*  - Connell Harte 09005210
*  - Paul McCarthy 16137604
*
* */

public class Main {

    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        words.add("duck");
        words.add("dog");
        words.add("cat");

        String[] filenames = {"simple-english.txt", "dino.txt", "words.txt"};


        WordSearchPuzzle simplePuzzle = new WordSearchPuzzle(filenames[0], 100, 8, 15);
        WordSearchPuzzle dinosaurPuzzle = new WordSearchPuzzle(filenames[1], 20, 5, 12);
        //words.txt too large to send in email so has been truncated.
        //WordSearchPuzzle largePuzzle = new WordSearchPuzzle(filenames[2], 100, 5, 20);
        WordSearchPuzzle userDefinedPuzzle = new WordSearchPuzzle(words);

        simplePuzzle.showWordSearchPuzzle();
        dinosaurPuzzle.showWordSearchPuzzle();
        userDefinedPuzzle.showWordSearchPuzzle();
        //largePuzzle.showWordSearchPuzzle();

        userDefinedPuzzle.showWordSearchSolution();


    }
}
